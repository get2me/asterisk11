# Dahdi, Libpri, Asterisk 11 LTS (выполнять скрипт в /usr/src)
wget http://mirror.freepbx.org/freepbx-2.11.0.42.tgz
wget http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-current.tar.gz
wget http://downloads.asterisk.org/pub/telephony/libpri/libpri-1.4-current.tar.gz
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-11-current.tar.gz
tar zxvf freepbx-*
mv freepbx.sh freepbx
tar zxvf dahdi-linux-complete*
tar zxvf libpri*
tar zxvf asterisk-*
cd dahdi-linux-complete* && make && make install && make config && cd ..
cd libpri-* && make && make install && cd ..
cd asterisk-* && ./configure --libdir=/usr/lib64 && make menuselect && make && make install && make samples && make config