adduser asterisk -M -c "Asterisk User"
chown asterisk. /var/run/asterisk
chown -R asterisk. /etc/asterisk
chown -R asterisk. /var/{lib,log,spool}/asterisk
chown -R asterisk. /usr/lib64/asterisk
chown -R asterisk. /var/www/
chown asterisk /var/lib/php/session/
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/httpd/conf/httpd.conf
sed -i 's/AllowOverride All/AllowOverride None/' /etc/httpd/conf/httpd.conf
export ASTERISK_DB_PW=amp109
mysqladmin -u root -pdbpass create asterisk 
mysqladmin -u root -pdbpass create asteriskcdrdb 
mysql -u root -pdbpass asterisk < SQL/newinstall.sql 
mysql -u root -pdbpass asteriskcdrdb < SQL/cdr_mysql_table.sql
mysql -u root -pdbpass -e "GRANT ALL PRIVILEGES ON asterisk.* TO asteriskuser@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';"
mysql -u root -pdbpass -e "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO asteriskuser@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';"
mysql -u root -pdbpass -e "flush privileges;"
reboot