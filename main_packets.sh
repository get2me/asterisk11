chkconfig --level 0123456 iptables off
service iptables stop
yum groupinstall core -y
yum groupinstall base -y
yum groupinstall "Development Tools" -y
yum install -y openssl-devel telnet libuuid-devel compat-libtermcap lynx make ncurses-devel sox newt-devel libxml2-devel audiofile-devel crontabs cronie cronie-anacron libtool uuid-devel sqlite-devel